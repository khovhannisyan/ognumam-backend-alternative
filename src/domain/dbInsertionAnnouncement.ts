import { Expose } from 'class-transformer';
import { AnnouncementBase } from './announcement';

export class DbInsertionAnnouncement extends AnnouncementBase {
  @Expose() category;
}
