import { EntityRepository, FindManyOptions, Raw, Repository } from 'typeorm';
import { CategoryEntity } from '../models/category.entity';

@EntityRepository(CategoryEntity)
export class CategoryRepository extends Repository<CategoryEntity> {

  public async getPagedWithCountAsync(q: string,
                        offset: number,
                        limit: number) {
    const searchQuery: FindManyOptions = {
      skip: offset,
      take: limit,
      order: { id: 'ASC' },
    };
    if (q) {
      searchQuery.where = { name: Raw(alias =>`LOWER(${alias}) Like '%${q.toLowerCase()}%'`) };
    }
    return  await super.findAndCount(searchQuery);
  }

  public async getSingleWithAnnouncements(catId: number) {
    return await super.findOne(
      { id: catId },
      { relations: ['announcements'] }
    );
  }
}