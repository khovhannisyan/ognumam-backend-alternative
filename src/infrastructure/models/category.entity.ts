import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AnnouncementEntity } from './announcement.entity';

@Entity()
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256, nullable: false })
  name: string;

  @OneToMany(
    () => AnnouncementEntity,
    a => a.category,
    {onDelete: 'RESTRICT'}
  )
  public announcements: AnnouncementEntity[];
}
