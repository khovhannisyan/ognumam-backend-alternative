import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../application/services/auth.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../infrastructure/models/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CredentialInterceptor implements NestInterceptor {
  constructor(private readonly authService: AuthService,
              @InjectRepository(UserEntity)
              private userRepository: Repository<UserEntity>,
  ) {
  }

  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const req = context.switchToHttp().getRequest();
    if (!req.user &&
      req.headers['authorization']
    ) {
      const header = req.headers['authorization'].split(' ');
      if (header[0] === 'Bearer') {
        const decodeToken = this.authService.decodeToken(header[1]);
        const user = await this.userRepository.findOne(
          { email: decodeToken['id'] },
        );
        if (!!user)
          req.user = user;
      }
    }
    return next.handle();
  }
}