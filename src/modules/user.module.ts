import { Module } from '@nestjs/common';
import { UserController } from '../API/controllers/user.controller';
import { UserService } from '../application/services/user.service';
import { AuthModule } from './auth.module';
import { UserEntity } from '../infrastructure/models/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]), AuthModule],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
