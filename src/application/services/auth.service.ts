import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';
import { Configs } from '../../config/config';
import { IJwtPayload } from '../interfaces/IJwtPayload';
import { User } from '../../domain/user';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../infrastructure/models/user.entity';
import { Repository } from 'typeorm';
import { Mapper } from '../extentions/mapper';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  public async getToken(payload: IJwtPayload) {
    return this.jwtService.sign(payload);
  }

  public decodeToken(token: string) {
    return this.jwtService.decode(token);
  }


  public async validateUser(
    payload: IJwtPayload,
    token: string,
  ): Promise<User> {
    if (!token) {
      return null;
    }
    if (this.jwtService.decode(token.toString(), Configs.privateKey as any)) {
      const user = await this.userRepository.findOne({ email: payload.id });
      if (!user || user.token !== token) {
        return null;
      }
      if (!AuthService.ifOutdated(user.updatedAt)) {
        await this.userRepository.save(user);
      }
      return Mapper.Map(User, user);
    }
    return null;
  }

  private static ifOutdated(date: Date): boolean {
    return date && Date.now() - date.getTime() > Configs.tokenTimeout;
  }
}
