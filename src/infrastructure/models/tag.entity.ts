import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AnnouncementEntity } from './announcement.entity';

@Entity()
export class TagEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256, nullable: false })
  name: string;
}
