import { Expose } from 'class-transformer';
import { CategoryDto } from './categoryDto';
import { LegalEntity } from '../../domain/legalEntityType';
import { UserDto } from './userDto';

export class AnnouncementDto {
  @Expose() name: string;
  @Expose() id;
  @Expose() category: CategoryDto;
  @Expose() description: string;
  @Expose() image: string;
  @Expose() address: string;
  @Expose() phone: string;
  @Expose() legalState: LegalEntity;
  @Expose() visibilityLevel: UserDto;
}
