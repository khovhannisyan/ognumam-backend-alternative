import { EntityRepository, FindManyOptions, Like, Raw, Repository } from 'typeorm';
import { CategoryEntity } from '../models/category.entity';
import { AnnouncementEntity } from '../models/announcement.entity';
import { UserRoles } from '../../API/types/userRoles';

@EntityRepository(AnnouncementEntity)
export class AnnouncementRepository extends Repository<AnnouncementEntity> {

  public async getPagedWithCountAsync(q: string,
                                      offset: number,
                                      limit: number,
                                      categoryId: number,
                                      visibility: UserRoles) {
    const announcementAlias = 'ann';
    const categoryAlias = 'category';
    let builder = super
      .createQueryBuilder(announcementAlias)
      .leftJoinAndMapOne(`${announcementAlias}.category`, "ann.category", categoryAlias)
      .limit(limit)
      .offset(offset)
      .orderBy("updated_At",'DESC');

    if (q) {
      builder = builder.where([
        { name: Raw(alias => `LOWER(${alias}) Like '%${q.toLowerCase()}%'`) },
        { description: Raw(alias => `LOWER(${alias}) Like '%${q.toLowerCase()}%'`) },
        { address: Raw(alias => `LOWER(${alias}) Like '%${q.toLowerCase()}%'`) },
        { category: { name: Raw(alias => `LOWER(${alias}) Like '%${q.toLowerCase()}%'`) } },
      ]);
    }
    if (categoryId) {
      builder = builder.andWhere(`${announcementAlias}.category_id = ${categoryId}`);
    }
    builder = builder.andWhere(`${announcementAlias}.visibility::int <= ${visibility}`);

    return await builder.getManyAndCount();
  }
}