import * as crypto from 'crypto';
import { Configs } from '../config/config';
import { Expose } from 'class-transformer';
import { LegalEntity } from './legalEntityType';
import { UserRole } from './userRoleType';

export class AnnouncementBase {
  @Expose() name: string;
  @Expose() description: string;
  @Expose() image: string;
  @Expose() address: string;
  @Expose() phone: string;
  @Expose() legalState: LegalEntity;
  @Expose() visibilityLevel: UserRole;

  public putImageName(): void {
    if (this.image)
      this.image = crypto
        .createHmac('sha256', Configs.imageSecret)
        .update(this.image.split('.')[0])
        .digest('hex');
  }
}
