import { AnnouncementBase } from './announcement';
import { Expose } from 'class-transformer';
import { Category } from './category';

export class DbQueryAnnouncement extends AnnouncementBase {
  @Expose() category: Category;
}
