import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class UserEditDto {
  @ApiModelProperty() readonly name: string;
  @ApiModelProperty() readonly surname: string;
  @ApiModelProperty() password: string;
  @ApiModelProperty() confirmPassword: string;
  @ApiModelProperty() readonly phone: string;
}
