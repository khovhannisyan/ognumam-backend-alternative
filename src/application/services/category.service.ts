import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Mapper } from '../extentions/mapper';
import { CategoryCreationDto } from '../models/categoryCreationDto';
import { Category } from '../../domain/category';
import { CategoryDto } from '../models/categoryDto';
import { PagedListHolder } from '../extentions/pagedListHolder';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '../../infrastructure/models/category.entity';
import { FindManyOptions, Like, Not, Raw, Repository } from 'typeorm';
import { CategoryRepository } from '../../infrastructure/repositories/category.repository';
import { of } from 'rxjs';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryRepository)
    private categoryRepository: CategoryRepository,
  ) {}

  public async addNewCategoryAsync(
    categoryCreationDto: CategoryCreationDto,
  ): Promise<CategoryDto> {
    if (await this.categoryRepository.count({ name: categoryCreationDto.name }))
      throw new ConflictException();

    const dbCategory: Category = Mapper.Map(Category, categoryCreationDto);
    const newCategory = await this.categoryRepository.save(dbCategory);
    return Mapper.Map(CategoryDto, newCategory as CategoryEntity);
  }

  public async editAsync(catId: number, name: string): Promise<CategoryDto> {
    if (await this.categoryRepository.count({ name: name, id: Not(catId) }))
      throw new ConflictException();

    const dbCategory = await this.categoryRepository.findOne(catId);
    if (dbCategory.name === name) return Mapper.Map(CategoryDto, dbCategory);

    dbCategory.name = name;
    await this.categoryRepository.save(dbCategory);

    return Mapper.Map(CategoryDto, dbCategory);
  }

  public async removeAsync(catId: number): Promise<void> {
    const dbCategory = await this.categoryRepository.getSingleWithAnnouncements(catId);

    if (dbCategory) {
      if (dbCategory.announcements.length) throw new BadRequestException();

      await this.categoryRepository.remove(dbCategory);
    }
  }

  public async getByIdAsync(id: number): Promise<CategoryDto> {
    const category = await this.categoryRepository.findOne(id);

    if (!category) throw new NotFoundException();

    return Mapper.Map(CategoryDto, category);
  }

  public async getPagedAsync(
    q: string,
    offset: number,
    limit: number,
  ): Promise<PagedListHolder<CategoryDto>> {
    if (limit <= 0)
      throw new BadRequestException();

    const [categories, count] = await this.categoryRepository.getPagedWithCountAsync(q, offset, limit);

    return new PagedListHolder(
      Mapper.MapList(CategoryDto, categories),
      limit,
      offset,
      count,
    );
  }
}
