import { Expose } from 'class-transformer';

export class Category {
  @Expose() name: string;
}
