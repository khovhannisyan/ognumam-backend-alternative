import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Request,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { UserDto } from '../../application/models/userDto';
import { UserService } from '../../application/services/user.service';
import { RolesGuard } from '../middleware/rolesGuard';
import { Roles } from '../validation/roles';
import { UserRoles } from '../types/userRoles';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Mapper } from '../../application/extentions/mapper';
import { UserRegistrationDto } from '../../application/models/userRegistrationDto';
import { UserEditDto } from '../../application/models/userEditDto';

@Controller('users')
@ApiTags('users')
export class UserController {
  constructor(private readonly userService: UserService) {
  }

  @Post('login')
  @Roles(UserRoles.guest)
  @UseGuards(RolesGuard)
  public async loginAsync(
    @Body('email') email: string,
    @Body('password') password: string,
  ): Promise<UserDto> {
    return await this.userService.loginAsync(email, password);
  }

  @Post('register')
  @Roles(UserRoles.guest)
  @UseGuards(RolesGuard)
  public async registerAsync(
    @Body() userRegistrationDto: UserRegistrationDto,
  ): Promise<UserDto> {
    return await this.userService.registerAsync(userRegistrationDto);
  }

  @Get('me')
  @Roles(UserRoles.user, UserRoles.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiBearerAuth()
  public async meAsync(@Request() req): Promise<UserDto> {
    if (!req.user) throw new UnauthorizedException();
    return Mapper.Map(UserDto, req.user);
  }

  @Patch()
  @Roles(UserRoles.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async editAsync(
    @Request() req,
    @Body() userEditDto: UserEditDto,
  ): Promise<UserDto> {
    if (userEditDto.password || userEditDto.confirmPassword)
      return await this.userService.editAsync(req.user.id, {
        password: userEditDto.password,
        confirmPassword: userEditDto.confirmPassword
      } as UserEditDto);
    else {
      userEditDto.password = null;
      userEditDto.confirmPassword = null;
      return await this.userService.editAsync(req.user.id, userEditDto)
    }
  }

  @Put('logout')
  @Roles(UserRoles.user, UserRoles.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async logout(
    @Request() req,
  ): Promise<void> {
    await this.userService.logoutAsync(req.user.id);
    await req.logout();
  }

  @Put('reset/sendEmail')
  @Roles(UserRoles.guest)
  @UseGuards(RolesGuard)
  public async sendResetEmail(
    @Body("email") email: string
  ): Promise<void> {
    await this.userService.sendResetEmail(email);
  }
  @Put('reset/submit')
  @Roles(UserRoles.guest)
  @UseGuards(RolesGuard)
  public async submitResetCode(
    @Body("code") code: string,
    @Body("email") email: string
  ): Promise<UserDto> {
    return await this.userService.checkResetCodeAndLogin(email, code.toString());
  }

  @Delete(':id')
  @Roles(UserRoles.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async removeAsync(@Param('id') id: string): Promise<void> {
    return await this.userService.removeAsync(parseInt(id));
  }
}
