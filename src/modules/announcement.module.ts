import { Module } from '@nestjs/common';
import { AnnouncementService } from '../application/services/announcement.service';
import { AnnouncementController } from '../API/controllers/announcement.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnnouncementEntity } from '../infrastructure/models/announcement.entity';
import { CategoryEntity } from '../infrastructure/models/category.entity';
import { CategoryRepository } from '../infrastructure/repositories/category.repository';
import { AuthService } from '../application/services/auth.service';
import { AuthModule } from './auth.module';
import { UserEntity } from '../infrastructure/models/user.entity';
import { AnnouncementRepository } from '../infrastructure/repositories/announcement.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([AnnouncementEntity, AnnouncementRepository]),
    TypeOrmModule.forFeature([CategoryEntity, CategoryRepository]),
    TypeOrmModule.forFeature([UserEntity]),
    AuthModule
  ],
  controllers: [AnnouncementController],
  providers: [AnnouncementService],
})
export class AnnouncementModule {}
