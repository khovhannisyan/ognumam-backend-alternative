import { Module } from '@nestjs/common';
import { AuthService } from '../application/services/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { Configs } from '../config/config';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from '../application/extentions/jwtStrategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../infrastructure/models/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: Configs.privateKey,
      publicKey: Configs.publicKey,
      signOptions: { expiresIn: Configs.tokenTimeout },
    }),
  ],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService, JwtStrategy],
})
export class AuthModule {}
