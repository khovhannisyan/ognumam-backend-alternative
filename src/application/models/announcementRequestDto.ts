import { Expose } from 'class-transformer';
import { ObjectId } from 'mongoose';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { ApiProperty } from '@nestjs/swagger';
import { LegalEntity } from '../../domain/legalEntityType';
import { UserRole } from '../../domain/userRoleType';

export class AnnouncementRequestDto {
  @ApiModelProperty() @Expose() name: string;
  @ApiModelProperty() @Expose() category: number;
  @ApiModelProperty() @Expose() description: string;
  @ApiModelProperty() @Expose() address: string;
  @ApiModelProperty() @Expose() phone: string;
  @ApiProperty({ enum: LegalEntity }) @Expose() legalState: LegalEntity;
}
