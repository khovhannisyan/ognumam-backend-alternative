import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Roles } from '../validation/roles';
import { UserRoles } from '../types/userRoles';
import { ApiTags } from '@nestjs/swagger';
import { PagedListHolder } from '../../application/extentions/pagedListHolder';
import { FileUploadInterceptor } from '../interceptors/fileInterceptor';
import { RolesGuard } from '../middleware/rolesGuard';
import { AuthGuard } from '@nestjs/passport';
import { AnnouncementService } from '../../application/services/announcement.service';
import { AnnouncementDto } from '../../application/models/announcementDto';
import { AnnouncementRequestDto } from '../../application/models/announcementRequestDto';
import { CredentialInterceptor } from '../interceptors/credentialInterceptor';

@Controller('announcements')
@ApiTags('announcements')
export class AnnouncementController {
  constructor(private readonly announcementService: AnnouncementService) {}

  @Post()
  @Roles(UserRoles.guest, UserRoles.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async addNewAnnouncementAsync(
    @Body() announcementCreationDto: AnnouncementRequestDto,
  ): Promise<AnnouncementDto> {
    return await this.announcementService.addNewAnnouncementAsync(
      announcementCreationDto,
    );
  }

  @Get()
  @Roles(UserRoles.guest, UserRoles.user, UserRoles.admin)
  @UseGuards(RolesGuard)
  @UseInterceptors(CredentialInterceptor)
  public async getAnnouncementsPagedAsync(
    @Req() req,
    @Query('categoryId') categoryId: string,
    @Query('q') q: string,
    @Query('offset') offset: string,
    @Query('limit') limit: string,
  ): Promise<PagedListHolder<AnnouncementDto>> {
    return await this.announcementService.getAnnouncementsPagedAsync(
      categoryId ? parseInt(categoryId) : null,
      q,
      parseInt(offset),
      parseInt(limit),
      req.user ? req.user.role : UserRoles.guest,
    );
  }

  @Get(':id')
  @Roles(UserRoles.guest, UserRoles.user, UserRoles.admin)
  @UseGuards(RolesGuard)
  public async getByIdAsync(@Param('id') id: string): Promise<AnnouncementDto> {
    return await this.announcementService.getByIdAsync(parseInt(id));
  }

  @Delete(':id')
  @Roles(UserRoles.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async deleteAsync(@Param('id') id: string): Promise<void> {
    await this.announcementService.deleteAsync(parseInt(id));
  }

  @Patch(':id')
  @Roles(UserRoles.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async editAsync(
    @Param('id') id: string,
    @Body() announcementEditDto: AnnouncementRequestDto,
  ): Promise<AnnouncementDto> {
    return await this.announcementService.editAsync(
      parseInt(id),
      announcementEditDto,
    );
  }

  @Put(':id/image')
  @UseInterceptors(FileUploadInterceptor)
  @Roles(UserRoles.guest, UserRoles.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async uploadPictureAsync(
    @Param('id') id: string,
    @UploadedFile() file,
  ): Promise<void> {
    await this.announcementService.uploadPictureAsync(
      parseInt(id),
      'announcements/' + file.filename,
    );
  }

  @Delete(':id/image')
  @Roles(UserRoles.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  public async deletePictureAsync(@Param('id') id: string): Promise<void> {
    await this.announcementService.deletePictureAsync(parseInt(id));
  }
}
