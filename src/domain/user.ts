import * as crypto from 'crypto';
import { Configs } from '../config/config';
import { Expose } from 'class-transformer';
import { AuthService } from '../application/services/auth.service';
import { UserRoles } from '../API/types/userRoles';
import { MailerService } from '@nestjs-modules/mailer';

export class User {
  @Expose() id;
  @Expose() name: string;
  @Expose() surname: string;
  @Expose() email: string;
  @Expose() password: string;
  @Expose() token: string;
  @Expose() phone: string;
  @Expose() balance: number;
  @Expose() role: UserRoles;
  @Expose() resetCode: string;
  @Expose() resetCodeExpiration: Date;

  public putRole(role: UserRoles): void {
    this.role = role;
  }

  public hashPassword(): void {
    if (this.password)
      this.password = crypto
        .createHmac('sha256', Configs.secret)
        .update(this.password)
        .digest('hex');
  }

  public checkPassword(password: string): boolean {
    const hashedPass = crypto
      .createHmac('sha256', Configs.secret)
      .update(password)
      .digest('hex');
    return hashedPass === this.password;
  }

  public checkResetCode(code: string): boolean {
    return code === this.resetCode && (new Date()).getUTCMilliseconds() - this.resetCodeExpiration.getUTCMilliseconds() <= 3600000;
  }

  public async generateToken(authService: AuthService): Promise<void> {
    this.token = await authService.getToken({
      id: this.email,
      role: this.role,
    });
  }

  public async sendResetEmail(mailer: MailerService) {
    var randomstring = require("randomstring");
    this.resetCode = randomstring.generate({length: 6, charset: "numeric"});
    this.resetCodeExpiration = new Date(Date.now());
    // TODO add when we have credentials
    // await mailer.sendMail({
    //   to: this.email,
    //   from: 'noreply@nestjs.com',
    //   subject: 'Password Reset Code',
    //   template: 'resetEmail.template.pug',
    //   context: {
    //     code: this.resetCode,
    //     name: `${this.name} ${this.surname}`
    //   },
    // });
  }

  public expireToken(): void {
    this.token = null;
  }
}
