import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { Mapper } from '../extentions/mapper';
import { DbQueryAnnouncement } from '../../domain/dbQueryAnnouncement';
import { DbInsertionAnnouncement } from '../../domain/dbInsertionAnnouncement';
import { AnnouncementDto } from '../models/AnnouncementDto';
import { PagedListHolder } from '../extentions/pagedListHolder';
import { AnnouncementRequestDto } from '../models/announcementRequestDto';
import { UserRoles } from '../../API/types/userRoles';
import { InjectRepository } from '@nestjs/typeorm';
import { AnnouncementEntity } from '../../infrastructure/models/announcement.entity';
import { Like, Repository } from 'typeorm';
import { UserRole } from '../../domain/userRoleType';
import { CategoryEntity } from '../../infrastructure/models/category.entity';
import { CategoryRepository } from '../../infrastructure/repositories/category.repository';
import { AnnouncementRepository } from '../../infrastructure/repositories/announcement.repository';

@Injectable()
export class AnnouncementService {
  constructor(
    @InjectRepository(AnnouncementRepository)
    private announcementRepository: AnnouncementRepository,
    private categoryRepository: CategoryRepository,
  ) {}

  public async addNewAnnouncementAsync(
    AnnouncementCreationDto: AnnouncementRequestDto,
  ): Promise<AnnouncementDto> {
    if (
      await this.announcementRepository.count({
        name: AnnouncementCreationDto.name,
      })
    )
      throw new ConflictException();

    if (await this.categoryRepository.count({id: AnnouncementCreationDto.category}) == 0)
      throw new BadRequestException();

    const dbAnnouncement: DbQueryAnnouncement = Mapper.Map(
      DbQueryAnnouncement,
      AnnouncementCreationDto,
    );
    dbAnnouncement.visibilityLevel = UserRole.admin;

    const newAnnouncementModel = await this.announcementRepository.save(
      dbAnnouncement,
    );

    return Mapper.Map(AnnouncementDto, newAnnouncementModel);
  }

  public async getAnnouncementsPagedAsync(
    categoryId,
    q,
    offset,
    limit,
    visibility: UserRoles,
  ): Promise<PagedListHolder<AnnouncementDto>> {

    const [Announcements, count] = await this.announcementRepository.getPagedWithCountAsync(q, offset, limit, categoryId, visibility);

    return new PagedListHolder(
      Mapper.MapList(AnnouncementDto, Announcements),
      limit,
      offset,
      count,
    );
  }

  public async getByIdAsync(id: number): Promise<AnnouncementDto> {
    const Announcement = await this.announcementRepository.findOne(id);

    if (!Announcement) throw new NotFoundException();

    return Mapper.Map(AnnouncementDto, Announcement);
  }

  public async deleteAsync(id: number): Promise<void> {
    const Announcement = await this.announcementRepository.delete({ id: id });

    if (!Announcement.affected) throw new NotFoundException();
  }

  public async editAsync(
    AnnouncementId: number,
    AnnouncementEditDto: AnnouncementRequestDto,
  ): Promise<AnnouncementDto> {
    const dbAnnouncement = await this.announcementRepository.findOne(
      AnnouncementId,
    );

    if (!dbAnnouncement) throw new NotFoundException();

    const AnnouncementDomain = Mapper.Map(
      DbInsertionAnnouncement,
      AnnouncementEditDto || dbAnnouncement,
      true,
    );

    Object.assign(dbAnnouncement, AnnouncementDomain);
    await this.announcementRepository.save(dbAnnouncement);

    return Mapper.Map(AnnouncementDto, dbAnnouncement);
  }

  public async uploadPictureAsync(
    AnnouncementId: number,
    filename: string,
  ): Promise<void> {
    const dbAnnouncement = await this.announcementRepository.findOne(
      AnnouncementId,
    );

    if (!dbAnnouncement) throw new NotFoundException();

    const AnnouncementDomain = Mapper.Map(
      DbInsertionAnnouncement,
      dbAnnouncement,
      true,
    );

    AnnouncementDomain.image = filename;

    Object.assign(dbAnnouncement, AnnouncementDomain);

    await this.announcementRepository.save(dbAnnouncement);
  }

  public async deletePictureAsync(AnnouncementId: number): Promise<void> {
    const dbAnnouncement = await this.announcementRepository.findOne(
      AnnouncementId,
      { relations: ['category'] },
    );

    if (!dbAnnouncement) throw new NotFoundException();

    dbAnnouncement.image = null;

    await this.announcementRepository.save(dbAnnouncement);
  }
}
