import { Expose } from 'class-transformer';

export class UserDto {
  @Expose() id;
  @Expose() name: string;
  @Expose() surname: string;
  @Expose() email: string;
  @Expose() token: string;
  @Expose() address: string;
  @Expose() phone: string;
}
