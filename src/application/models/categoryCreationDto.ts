import { Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class CategoryCreationDto {
  @ApiModelProperty() @Expose() name: string;
}
