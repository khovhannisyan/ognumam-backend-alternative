import {
  Column,
  Entity, JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { LegalEntity } from '../../domain/legalEntityType';
import { UserRole } from '../../domain/userRoleType';
import { CategoryEntity } from './category.entity';
import { TagEntity } from './tag.entity';

@Entity()
export class AnnouncementEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256, nullable: false })
  name: string;

  @Column({ length: 256, nullable: true })
  address: string;

  @Column({ length: 13, nullable: true })
  phone: string;

  @Column({ length: 256, nullable: true })
  link: string;

  @Column({ length: 256, nullable: true })
  image: string;

  @Column({ type: "text", nullable: true })
  description: string;

  @Column({ type: 'int', enum: UserRole, default: UserRole.guest, name: "visibility" })
  visibilityLevel: UserRole;

  @Column({
    type: 'int',
    enum: LegalEntity,
    default: LegalEntity.individual,
  })
  legalState: LegalEntity;

  @ManyToOne(
    () => CategoryEntity,
    c => c.announcements
  )
  @JoinColumn({name: "category_id"})
  category: CategoryEntity;

  @UpdateDateColumn({name: "updated_at"})
  updatedAt: Date;

  @ManyToMany(() => TagEntity)
  @JoinTable()
  tags: TagEntity[];
}
