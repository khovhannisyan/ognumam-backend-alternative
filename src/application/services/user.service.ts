import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserDto } from '../models/userDto';
import { UserRegistrationDto } from '../models/userRegistrationDto';
import { User } from '../../domain/user';
import { Mapper } from '../extentions/mapper';
import { AuthService } from './auth.service';
import { UserRoles } from '../../API/types/userRoles';
import { UserEditDto } from '../models/userEditDto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../infrastructure/models/user.entity';
import { Repository } from 'typeorm';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private readonly authService: AuthService,
    private readonly mailerService: MailerService
  ) {
  }

  public async registerAsync(
    userRegistrationDto: UserRegistrationDto,
  ): Promise<UserDto> {
    if (userRegistrationDto.password !== userRegistrationDto.confirmPassword) {
      throw new BadRequestException();
    }
    if (await this.userRepository.count({ email: userRegistrationDto.email }))
      throw new ConflictException();

    const dbUser: User = Mapper.Map(User, userRegistrationDto);

    dbUser.putRole(UserRoles.user);
    dbUser.hashPassword();
    await dbUser.generateToken(this.authService);

    const newUserModel = (await this.userRepository.save(dbUser)) as UserEntity;
    return Mapper.Map(UserDto, newUserModel);
  }

  public async loginAsync(email: string, password: string): Promise<UserDto> {
    const user = await this.userRepository.findOne({ email: email });

    if (!user) throw new NotFoundException();

    const dbUser: User = Mapper.Map(User, user);

    if (!dbUser.checkPassword(password)) throw new ForbiddenException();

    await dbUser.generateToken(this.authService);

    Object.assign(user, dbUser);
    await this.userRepository.save(user);

    return Mapper.Map(UserDto, user);
  }

  public async sendResetEmail(email: string): Promise<void> {
    const user = await this.userRepository.findOne({email: email});

    if (!user) throw new NotFoundException();

    const dbUser: User = Mapper.Map(User, user);

    await dbUser.sendResetEmail(this.mailerService);

    Object.assign(user, dbUser);
    await this.userRepository.save(dbUser);
  }

  public async checkResetCodeAndLogin(email: string, code: string): Promise<UserDto> {
    const dbUser = await this.userRepository.findOne({ email: email });

    if (!dbUser) throw new NotFoundException();

    const userDomain: User = Mapper.Map(User, dbUser);

    if (!userDomain.checkResetCode(code)) throw new ForbiddenException();

    await userDomain.generateToken(this.authService);


    Object.assign(dbUser, userDomain);

    dbUser.resetCode = null;
    dbUser.resetCodeExpiration = null;

    await this.userRepository.save(dbUser);

    return Mapper.Map(UserDto, dbUser);
  }

  public async logoutAsync(id: number): Promise<void> {
    const user = await this.userRepository.findOne(id);

    if (!user) throw new NotFoundException();

    const dbUser: User = Mapper.Map(User, user);

    dbUser.expireToken();

    Object.assign(user, dbUser);

    await this.userRepository.save(dbUser);
  }

  public async editAsync(
    userId: number,
    userEditDto: UserEditDto,
  ): Promise<UserDto> {
    if (
      userEditDto.password &&
      userEditDto.password !== userEditDto.confirmPassword
    ) {
      throw new BadRequestException();
    }
    const dbUser = await this.userRepository.findOne(userId);

    const userDomain = Mapper.Map(User, userEditDto, true);

    if (userDomain.password) {
      if (!userDomain.checkPassword(userDomain.password)) userDomain.hashPassword();
      else throw new BadRequestException();
    }

    Object.assign(dbUser, userDomain);
    await this.userRepository.save(dbUser);

    return Mapper.Map(UserDto, dbUser);
  }

  public async getByIdAsync(id: number): Promise<UserDto> {
    const user = await this.userRepository.findOne(id);

    if (!user) throw new NotFoundException();

    return Mapper.Map(UserDto, user);
  }

  public async removeAsync(id: number): Promise<void> {
    const user = await this.userRepository.delete({ id: id });

    if (!user.affected) throw new NotFoundException();
  }
}
