import { Module } from '@nestjs/common';
import { CategoryService } from '../application/services/category.service';
import { CategoryController } from '../API/controllers/category.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryEntity } from '../infrastructure/models/category.entity';
import { CategoryRepository } from '../infrastructure/repositories/category.repository';

@Module({
  imports: [TypeOrmModule.forFeature([CategoryEntity, CategoryRepository])],
  controllers: [CategoryController],
  providers: [CategoryService],
})
export class CategoryModule {}