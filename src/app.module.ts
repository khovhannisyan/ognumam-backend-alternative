import { Module } from '@nestjs/common';
import { AuthModule } from './modules/auth.module';
import { UserModule } from './modules/user.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import * as path from 'path';
import { CategoryModule } from './modules/category.module';
import { AnnouncementModule } from './modules/announcement.module';
import { databaseProvider } from './infrastructure/providers/database.provider';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';

@Module({
  imports: [
    TypeOrmModule.forRoot(databaseProvider.getTypeOrmConfig()),
    ServeStaticModule.forRoot({
      rootPath: path.join(__dirname, '..', 'public'),
    }),
    MailerModule.forRoot({
      transport: 'smtps://karahov@gmail.com:pass@smtp.domain.com',
      preview: true,
      template: {
        dir: __dirname + '/public/templates',
        adapter: new PugAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    UserModule,
    CategoryModule,
    AnnouncementModule,
    AuthModule,
  ],
})
export class AppModule {}
