import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserRoles } from '../../API/types/userRoles';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256, nullable: false })
  name: string;

  @Column({ length: 256, nullable: true })
  surname: string;

  @Column({ length: 256, nullable: false })
  password: string;

  @Column({ nullable: true })
  token: string;

  @Column({ length: 100, nullable: false })
  email: string;

  @Column({ length: 6, nullable: true, name: "reset_code" })
  resetCode: string;

  @Column({ type: "timestamp with time zone", nullable: true, name: "reset_code_expiration" })
  resetCodeExpiration: Date;

  @Column({ type: 'int', enum: UserRoles, default: UserRoles.user })
  role: UserRoles;

  @UpdateDateColumn()
  updatedAt: Date;
}
